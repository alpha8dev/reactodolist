import React, { Component } from "react";

import "./App.scss";

import MuiThemeProvider from "@material-ui/core/styles/MuiThemeProvider";
import theme from "./theme";
import Grid from "@material-ui/core/Grid";
import TaskContainer from "./TaskContainer";
import TaskForm from "./TaskForm";

class App extends Component {
  //ES6 way....not useful according to some people ;)
  constructor(props) {
    super(props);

    this.state = {
      firstname: "",
      lastname: "",
      taskOver: false,
      task: "",
      taskList: [],
      isEditing: false,
      currentlyEditedTask: 0,
      indexOfTaskBeingEdited: undefined,
      anotherTaskIsBeingEdited: false
    };
  }

  //Generate a unique ID for the tasks list
  generateID = () => {
    return (
      "_" +
      Math.random()
        .toString(36)
        .substr(2, 9)
    );
  };

  //Generic method for handling state
  handleChange = e => {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  };

  //handling if a task is done or not
  handleTaskChange = () => {
    this.setState(prevState => ({ taskOver: !prevState.taskOver }));
  };

  //form is located in TaskForm component
  handleTaskInput = e => {
    const { value } = e.target;
    this.setState({ task: value });
  };

  //handling the TaskForm submit
  handleSubmit = e => {
    e.preventDefault();
    const id = this.generateID();
    this.setState(({ taskList, task }) => ({
      taskList: [{ id: id, name: task }, ...taskList]
    }));
    this.setState({ task: "" });
  };

  getTaskIndexById(id){
    const taskList = this.state.taskList;
    return taskList.findIndex(task => task.id === id /* condition */);
  }

  handleEditCancel = id => {
    const taskList = this.state.taskList;
    taskList[this.getTaskIndexById(id)].isBeingEdited = false;
    this.setState({anotherTaskIsBeingEdited: false})
    this.setState({indexOfTaskBeingEdited: undefined})
    this.setState({taskList});
  }

  //toggling on the edit mode for a particular task
  handleEdit = id => {
    const taskList = this.state.taskList;
    const index = this.getTaskIndexById(id);

    if(this.state.anotherTaskIsBeingEdited){
      taskList[this.state.indexOfTaskBeingEdited].isBeingEdited = false;
      this.setState({taskList})
    }

    taskList[index].isBeingEdited = true;
    this.setState({anotherTaskIsBeingEdited: true})
    this.setState({indexOfTaskBeingEdited: index})
    this.setState({taskList});
  };

  //handle the value change of textfield (edit mode)
  handleTaskEditing = (e, id) => {
    const { value } = e.target;
    const taskList = this.state.taskList;
    const index = this.getTaskIndexById(id);
    
    taskList[index].currentlyEditedText = value;

    this.setState({ taskList });
    //this.setState({ currentlyEditedTaskName: value });
  };

  //ez to get, right?
  saveEditedTask = id => {
    const taskList = this.state.taskList;
    taskList[this.getTaskIndexById(id)].name = this.state.taskList[this.getTaskIndexById(id)].currentlyEditedText;
    taskList[this.getTaskIndexById(id)].isBeingEdited = false;
    this.setState({indexOfTaskBeingEdited: undefined})
    this.setState({taskList});
  }

  consoleLogTheState = () => {
    console.log(this.state);
  }

  //delete a task from the list
  deleteTask = id => {
    const taskList = this.state.taskList;
    this.setState({ taskList: taskList.filter(task => task.id !== id) });
  };

  render() {
    //let task = this.state.taskOver ? "Tâche finie" : "tâche pas finie";
    return (
      <MuiThemeProvider theme={theme}>
        <div className="container">
          <Grid container justify="center">
            <TaskContainer
              taskList={this.state.taskList}
              deleteTask={this.deleteTask}
              isEditing={this.state.task.isEditing}
              handleEdit={this.handleEdit}
              handleTaskEditing={this.handleTaskEditing}
              saveEditedTask={this.saveEditedTask}
              handleEditCancel={this.handleEditCancel}
            />
          </Grid>
          <Grid container justify="center">
            <TaskForm
              handleSubmit={this.handleSubmit}
              handleTaskInput={this.handleTaskInput}
              taskName={this.state.task}
            />
          </Grid>
          <button onClick={this.consoleLogTheState}>log</button>
        </div>
      </MuiThemeProvider>
    );
  }
}

export default App;
